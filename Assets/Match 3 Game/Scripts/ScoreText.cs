﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ScoreText : MonoBehaviour
{

    public Text scoreText;
    public Text highscoreText;

    // Start is called before the first frame update
    void Start()
    {
        scoreText.text = GameSettings.Instance.LastScore.ToString();
        highscoreText.text = GameSettings.Instance.Highscore.ToString();
    }

}
