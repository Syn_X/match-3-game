﻿using System.Collections;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.UI;

public class PauseActionManager : MonoBehaviour
{
    [SerializeField]
    private Image m_ButtonImage;

    [SerializeField]
    private Sprite m_PauseSprite;

    [SerializeField]
    private Sprite m_PlaySprite;

    [SerializeField]
    private GameObject m_GamePausedPanel;
    private Image m_GamePausedImage;

    public UnityEvent m_PauseGameEvent;
    public UnityEvent m_PlayGameEvent;

    private bool m_IsPressed { get; set; }

    // Start is called before the first frame update
    void Start()
    {

        if (m_ButtonImage == null)
        {
            m_ButtonImage = GetComponent<Image>();
        }
        if (m_PauseSprite == null)
        {
            m_PauseSprite = Resources.Load<Sprite>("Sprites/pause_icon");
        }
        if (m_PlaySprite == null)
        {
            m_PlaySprite = Resources.Load<Sprite>("Sprites/play_icon");
        }

        m_GamePausedImage = m_GamePausedPanel.GetComponent<Image>();
        m_GamePausedPanel.SetActive(false);

        m_IsPressed = false;
    }
    public void ButtonPressed()
    {
        if (m_IsPressed)
        {
            m_ButtonImage.sprite = m_PauseSprite;
            PlayGame();
        } else
        {
            m_ButtonImage.sprite = m_PlaySprite;
            PauseGame();
        }
        m_IsPressed = !m_IsPressed;
    }

    private void PauseGame()
    {
        m_PauseGameEvent.Invoke();
        StartCoroutine(FadePausePanelIn());

    }

    private void PlayGame()
    {
        m_PlayGameEvent.Invoke();
        StartCoroutine(FadePausePanelOut());
    }

    private IEnumerator FadePausePanelOut()
    {
        var tempColor = m_GamePausedImage.color;

        for (float f = 1f; f >= 0; f -= 0.1f)
        {
            tempColor.a = f;
            m_GamePausedImage.color = tempColor;

            yield return new WaitForSeconds(0.01f);
        }
        tempColor.a = 0.0f;
        m_GamePausedImage.color = tempColor;
        m_GamePausedPanel.gameObject.SetActive(false);
    }

    private IEnumerator FadePausePanelIn()
    {
        m_GamePausedPanel.gameObject.SetActive(true);

        var tempColor = m_GamePausedImage.color;

        for (float f = 1f; f >= 0; f -= 0.1f)
        {
            tempColor.a = 1f - f;
            m_GamePausedImage.color = tempColor;

            yield return new WaitForSeconds(0.01f);
        }

        tempColor.a = 1.0f;
        m_GamePausedImage.color = tempColor;
    }
}
