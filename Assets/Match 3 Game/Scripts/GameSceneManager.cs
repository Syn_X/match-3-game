﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class GameSceneManager : MonoBehaviour
{
    public Animator crossfade;
    private float transitionTime = 0.5f;

    public void GameOver()
    {
        StartCoroutine(LoadNextScene());
    }

    private IEnumerator LoadNextScene()
    {
        crossfade.SetTrigger("CrossfadeStart");
        yield return new WaitForSeconds(transitionTime);

        SceneManager.LoadScene("GameOverScene");
    }
}
