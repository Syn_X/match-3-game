﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.EventSystems;
using UnityEngine.UI;

public class TileManager : MonoBehaviour, IPointerDownHandler, IPointerUpHandler
{
    public Animator m_Animator { get; private set;}
    public Image m_Image { get; private set; }

    public UnityEvent tileSwipeEvent;

    public UnityEvent powerUpTouchEvent;

    private Vector2 initialTouchPosition;
    private Vector2 finalTouchPosition;

    public Vector2 m_SwipeDirection { get; private set; }

    public int xIndex { get; set; }
    public int yIndex { get; set; }

    public bool IsSelected { get; set; }
    public bool IsPowerUp { get; set; }



    #region Unity functions
    void Start()
    {
        m_Animator = GetComponentInChildren<Animator>();
        m_Image = GetComponentInChildren<Image>();

        // Link OnSwipe event 
        tileSwipeEvent = new UnityEvent();
        tileSwipeEvent.AddListener( () => BoardManager.Instance.OnTileSwipe(xIndex, yIndex, m_SwipeDirection));

        powerUpTouchEvent = new UnityEvent();
        powerUpTouchEvent.AddListener(() => BoardManager.Instance.OnPowerUpTouch(xIndex, yIndex));

        m_SwipeDirection = Vector2.zero;
    }

    #endregion

    #region Interface functions

    public void OnPointerDown(PointerEventData eventData)
    {
        if(BoardManager.Instance.m_GameState != GameState.Playing)
        {
            return;
        }

        initialTouchPosition = eventData.position;
        finalTouchPosition = Vector2.zero;

        SetSelected();
    }

    public void OnPointerUp(PointerEventData eventData)
    {
        if (BoardManager.Instance.m_GameState != GameState.Playing)
        {
            return;
        }

        SetDeselected();

        finalTouchPosition = eventData.position;

        if (IsPowerUp)
        {
            powerUpTouchEvent.Invoke();
        }
        else
        {
            float dragDistance = Mathf.Max(Mathf.Abs(finalTouchPosition.y - initialTouchPosition.y), Mathf.Abs(finalTouchPosition.x - initialTouchPosition.x));

            // Swipe not valid conditions
            if (m_Image.sprite == null || BoardManager.Instance.m_IsShifting || dragDistance * 2 < m_Image.rectTransform.sizeDelta.x)
            {
                return;
            }
            else
            {
                // Swipe is valid, start swapping tiles
                m_SwipeDirection = CalculateSwipeDirection();
                tileSwipeEvent.Invoke();
            }
        }
    }

    #endregion

    #region State functions

    public void SetSelected()
    {
        IsSelected = true;
        m_Animator.SetTrigger("isSelected");
    }

    public void SetDeselected()
    {
        IsSelected = false;
        m_Animator.SetTrigger("isNotSelected");
    }

    public void ResetTileState()
    {
        SetDeselected();
        IsSelected = false;
        IsPowerUp = false;
        m_SwipeDirection = Vector2.zero;
        initialTouchPosition = Vector2.zero;
        finalTouchPosition = Vector2.zero;
    }
    #endregion

    #region Utilities 

    private Vector2 CalculateSwipeDirection()
    {
        float angle = CalculateSwipeAngle();
        if (angle > 315 || angle <= 45)
        {
            return Vector2.right;
        } else if (angle > 45 && angle <= 135)
        {
            return Vector2.up;
        } else if (angle > 135 && angle <= 225)
        {
            return Vector2.left;
        } else
        {
            return Vector2.down;
        }
    }

    // Returns the angle of the swipe in the [0°, 360°] range
    private float CalculateSwipeAngle()
    {
        float angle = Mathf.Atan2(finalTouchPosition.y - initialTouchPosition.y, finalTouchPosition.x - initialTouchPosition.x) * Mathf.Rad2Deg;
        return angle < 0 ? angle + 360 : angle;
    }

    #endregion
}