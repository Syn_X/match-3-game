﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.UI;

public class TimerManager : MonoBehaviour
{
    [SerializeField]
    private Text m_TimerLabel;

    public Image m_FreezeTimerImage;

    private float m_TimeLeft = 0;

    public UnityEvent m_TimeIsUpEvent = new UnityEvent();

    private bool timerIsPaused = false;
    private bool timeEnded = false;

    private bool m_TimerFrozen = false;
    private float m_FrozenSecondsLeft = 0;

    // Start is called before the first frame update
    void Start()
    {
        if (m_TimerLabel == null)
        {
            foreach (var element in FindObjectsOfType<Text>())
            {
                if (element.name.Equals("TimerLabel")) m_TimerLabel = element;
            }
        }
        m_TimeLeft = GameSettings.Instance.PlaytimeInSeconds;
    }

    // Update is called once per frame
    void Update()
    {
        if (!timerIsPaused &&!timeEnded)
        {
            if (m_TimerFrozen)
            {
                m_FrozenSecondsLeft -= Time.deltaTime;

                Color temp = m_FreezeTimerImage.color;
                temp.a = m_FrozenSecondsLeft;
                m_FreezeTimerImage.color = temp;

                if (m_FrozenSecondsLeft < 0)
                {
                    UnfreezeTimer();
                    m_TimerFrozen = false;
                }
            } else
            {
                m_TimeLeft -= Time.deltaTime;
                UpdateTimerLabel();
                if (m_TimeLeft < 0)
                {
                    TimeIsUp();
                    timeEnded = true;
                }
            }
        }
    }

    #region Behaviour related functions

    void UpdateTimerLabel()
    {
        if (m_TimeLeft < 0)
        {
            m_TimerLabel.text = "--:--";
        } else
        {
            string seconds = (m_TimeLeft % 60).ToString("00");
            string minutes;
            if (seconds.Equals("60"))
            {
                seconds = "00";
                minutes = Mathf.Floor(m_TimeLeft / 60 + 1).ToString("00");
            } else
            {
                minutes = Mathf.Floor(m_TimeLeft / 60).ToString("00");
            }
            m_TimerLabel.text = minutes + ":" + seconds;
        }
    }

    private void TimeIsUp()
    {
        m_TimeIsUpEvent.Invoke();
    }

    public void GameOver()
    {
        m_TimeLeft = -1;
    }

    public void PauseTimer()
    {
        timerIsPaused = true;
    }

    public void UnpauseTimer()
    {
        timerIsPaused = false;
    }

    #endregion

    #region Freeze power up functions


    public void FreezeTimer()
    {
        m_FrozenSecondsLeft += GameSettings.Instance.FreezeTime;
        m_TimerFrozen = true;
    }

    private void UnfreezeTimer()
    {
        m_TimerFrozen = false;
    }

    #endregion
}
