﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class MenuSceneManager : MonoBehaviour
{
    public Animator crossfade;

    public Animator bombAnimator;
    public Animator freezeAnimator;

    private float transitionTime = 0.5f;

    public void StartGame()
    {
        if(GameSettings.Instance.PlayerHasSelectedPowerUp == false)
        {
            bombAnimator.SetTrigger("Shake");
            freezeAnimator.SetTrigger("Shake");
        } else
        {
            StartCoroutine(LoadNextScene());
        }
    }

    private IEnumerator LoadNextScene()
    {
        crossfade.SetTrigger("CrossfadeStart");
        yield return new WaitForSeconds(transitionTime);

        SceneManager.LoadScene("GameScene");
    }
}
