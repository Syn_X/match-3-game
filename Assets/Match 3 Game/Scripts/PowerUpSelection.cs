﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class PowerUpSelection : MonoBehaviour
{
    [SerializeField]
    private Image m_BombPanelImage;
    [SerializeField]
    private Image m_FreezePanelImage;
    [SerializeField]
    private BombSelectionManager m_BombManager;
    [SerializeField]
    private FreezeSelectionManager m_FreezeManager;
    [SerializeField]
    private Image m_BombImage;
    [SerializeField]
    private Image m_FreezeImage;

    private Color m_SelectedColor = Color.yellow;
    private Color m_DeselectedColor = Color.white;

    public void Awake()
    {
        m_SelectedColor.a = 0.75f;
        m_DeselectedColor.a = 0f;
    }

    public void Start()
    {
        if (GameSettings.Instance.PlayerHasSelectedPowerUp)
        {
            if (GameSettings.Instance.IsPowerUpBomb)
            {
                SetBombSelected();
            } else
            {
                SetFreezeSelected();
            }
        }
    }

    public void SetBombSelected()
    {
        m_BombManager.bombSelected = true;
        m_FreezeManager.freezeSelected = false;


        m_FreezeManager.animator.SetBool("Selected", false);
        m_FreezeManager.animator.enabled = false;

        m_BombManager.animator.enabled = true;
        m_BombManager.animator.SetBool("Selected", true);

        m_FreezePanelImage.color = m_DeselectedColor;
        m_BombPanelImage.color = m_SelectedColor;

        GameSettings.Instance.SelectedPowerUp = m_BombImage.sprite;
        GameSettings.Instance.IsPowerUpBomb = true;
        GameSettings.Instance.PlayerHasSelectedPowerUp = true;
    }

    public void SetFreezeSelected()
    {
        m_BombManager.bombSelected = false;
        m_FreezeManager.freezeSelected = true;

        m_FreezeManager.animator.enabled = true;
        m_FreezeManager.animator.SetBool("Selected", true);

        m_BombManager.animator.SetBool("Selected", false);
        m_BombManager.animator.enabled = false;

        m_FreezePanelImage.color = m_SelectedColor;
        m_BombPanelImage.color = m_DeselectedColor;

        GameSettings.Instance.SelectedPowerUp = m_FreezeImage.sprite;
        GameSettings.Instance.IsPowerUpBomb = false;
        GameSettings.Instance.PlayerHasSelectedPowerUp = true;
    }

}
