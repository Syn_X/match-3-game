﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.EventSystems;

public class BombSelectionManager : MonoBehaviour, IPointerUpHandler, IPointerDownHandler
{
    public UnityEvent m_BombSelectedEvent;

    public Animator animator;

    public bool bombSelected = false;

    private void Start()
    {
        animator = GetComponentInChildren<Animator>();
    }

    #region Interface functions

    public void OnPointerUp(PointerEventData eventData)
    {
        if (bombSelected) return;

        bombSelected = !bombSelected;

        m_BombSelectedEvent.Invoke();
    }


    public void OnPointerDown(PointerEventData eventData)
    {

    }

    #endregion

}
