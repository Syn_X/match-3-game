﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.EventSystems;

public class FreezeSelectionManager : MonoBehaviour, IPointerUpHandler, IPointerDownHandler
{
    public UnityEvent m_FreezeSelectedEvent;

    public Animator animator;

    public bool freezeSelected = false;

    private void Start()
    {
        animator = GetComponentInChildren<Animator>();
    }

    #region Interface functions

    public void OnPointerUp(PointerEventData eventData)
    {
        if (freezeSelected) return;

        freezeSelected = !freezeSelected;
        m_FreezeSelectedEvent.Invoke();
    }

    public void OnPointerDown(PointerEventData eventData)
    {

    }


    #endregion
}
