﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.UI;

public class BoardManager : MonoBehaviour
{
	#region Variables

	[HideInInspector]
	public GameState m_GameState;

	private static BoardManager _instance;
	public static BoardManager Instance { get { return _instance; } }

	public List<Sprite> characters = new List<Sprite>();
	public Sprite transparentSprite;
	public GameObject tile;
	private int xSize, ySize;
	private float powerUpChance = 0.0f;
	private int m_MatchMinSize = 3;
	public Text m_ScoreLabel;
	public Text m_PointsAddedLabel;
	public CanvasGroup m_PointsCanvasGroup;
	private Sprite m_PowerUpSprite;
	private bool m_IsPowerUpBomb = false;

	public UnityEvent m_GameOverEvent;


	private GameObject[,] tiles;
	public GameObject[,] Tiles { get => this.tiles; }

	public bool m_IsShifting { get; set; }

	private List<Tile> m_FirstPossibleMatch;
	private List<Match> m_FoundMatches;
	private bool ShouldCheckMatches { get; set; }
	private bool IsProcessing { get; set; }

	private int m_Score;
	private int m_PointsToAdd = 0;

	private TimerManager timer;

	#endregion

	#region Unity functions

	private void Awake()
	{
		if (_instance != null && _instance != this)
		{
			Destroy(this.gameObject);
		}
		else
		{
			_instance = this;
		}
	}

	void Start()
	{
		m_FirstPossibleMatch = new List<Tile>();
		m_FoundMatches = new List<Match>();

		// Retrieve settings
		xSize = GameSettings.Instance.BoardWidth;
		ySize = GameSettings.Instance.BoardHeight;
		powerUpChance = GameSettings.Instance.PowerUpChance;
		m_MatchMinSize = GameSettings.Instance.MatchMinSize;

		m_PowerUpSprite = GameSettings.Instance.SelectedPowerUp;
		m_IsPowerUpBomb = GameSettings.Instance.IsPowerUpBomb;

		timer = FindObjectOfType<TimerManager>();

		m_Score = 0;
		m_ScoreLabel.text = m_Score.ToString();
		m_PointsCanvasGroup.alpha = 0;

		m_FirstPossibleMatch = new List<Tile>();
		
		do {
			CreateBoard();
			m_FirstPossibleMatch = CheckFirstPossibleMatch();
		} while (m_FirstPossibleMatch.Count < 3);

		m_GameState = GameState.Playing;

		ShouldCheckMatches = false;
		IsProcessing = false;
	}

	private void Update()
	{
		m_ScoreLabel.text = m_Score.ToString();

		if (IsProcessing) return;

		switch (m_GameState)
		{
			case GameState.Playing:
				{
					if(m_PointsToAdd > 0)
					{
						StartCoroutine(SendPointsToScore(m_PointsToAdd));
						m_PointsToAdd = 0;
					}

					if (m_FirstPossibleMatch.Count < m_MatchMinSize)
					{
						ShouldCheckMatches = false;
						SetGameEnded();
					}
					else
					{
						// Play
						if (ShouldCheckMatches)
						{
							ShouldCheckMatches = false;
							CheckMatches();
						}

					}
				}
				break;
			case GameState.OnPause:
				break;
			case GameState.Ended:
				// GAME OVER
				break;
			default:
				break;
		}

	}

	#endregion

	#region Match finding and management

	private void IncrementScore(int points)
	{
		m_PointsToAdd += points;
	}

	private void CheckMatches()
	{
		m_FoundMatches.Clear();
		m_FoundMatches.AddRange(SearchForHorizontalMatches());
		m_FoundMatches.AddRange(SearchForVerticalMatches());

		if (m_FoundMatches.Count > 0)
		{
			IsProcessing = true;
			for (int i = 0; i < m_FoundMatches.Count; i++)
			{
				Match m = m_FoundMatches[i];
				if (m.direction.x > 0)
				{
					ProcessHorizontalMatch(m);
				}
				else if (m.direction.y > 0)
				{
					ProcessVerticalMatch(m);
				}
			}
			IsProcessing = false;
			CheckMatches();
		}
		m_FirstPossibleMatch = CheckFirstPossibleMatch();
	}

	private void ProcessHorizontalMatch(Match match)
	{
		for (int i = match.x; i < match.x + match.matchCounter; i++)
		{
			//TileManager tileManager = GetTileManager(i, match.y);
			//tileManager.IsToDelete = true;
			SetTileSprite(i, match.y, transparentSprite);
			FillEmptyTile(i, match.y);
		}
		IncrementScore(match.points);
	}

	private void ProcessVerticalMatch(Match match)
	{
		for (int i = match.y; i < match.y + match.matchCounter; i++)
		{
			//TileManager tileManager = GetTileManager(match.x, i);
			//tileManager.IsToDelete = true;
			SetTileSprite(match.x, i, transparentSprite);
			FillEmptyTile(match.x, i);
		}
		IncrementScore(match.points);
	}

	// Check if there's a possible match in the board (return the first match)
	private List<Tile> CheckFirstPossibleMatch()
	{
		m_FirstPossibleMatch.Clear();

		List<Tile> possibleMatches = new List<Tile>();

		for (int i = 0; i < xSize; i++)
		{
			for (int j = 0; j < ySize; j++)
			{
				// It should never happen, but check anyway 
				if (GetTileSprite(i, j) == null || GetTileSprite(i, j) == transparentSprite) continue;

				// O
				// X
				// X
				if (j < ySize - 2)
				{
					if (GetTileSprite(i, j) == GetTileSprite(i, j + 1))
					{
						// Top
						if (j < ySize - 3)
						{
							if (GetTileSprite(i, j) == GetTileSprite(i, j + 3))
							{
								possibleMatches.Add(new Tile(i, j));
								possibleMatches.Add(new Tile(i, j + 1));
								possibleMatches.Add(new Tile(i, j + 3));
								return possibleMatches;
							}
						}
						// Right
						if (i < xSize - 1)
						{
							if (GetTileSprite(i, j) == GetTileSprite(i + 1, j + 2))
							{
								possibleMatches.Add(new Tile(i, j));
								possibleMatches.Add(new Tile(i, j + 1));
								possibleMatches.Add(new Tile(i + 1, j + 2));
								return possibleMatches;
							}
						}
						// Left
						if (i > 0)
						{
							if (GetTileSprite(i, j) == GetTileSprite(i - 1, j + 2))
							{
								possibleMatches.Add(new Tile(i, j));
								possibleMatches.Add(new Tile(i, j + 1));
								possibleMatches.Add(new Tile(i - 1, j + 2));
								return possibleMatches;
							}
						}
					}
				}

				// X
				// X
				// O
				if (j < ySize - 2)
				{
					if (GetTileSprite(i, j + 1) == GetTileSprite(i, j + 2))
					{
						// Left
						if (i > 0 && j > 0 && GetTileSprite(i - 1, j) == GetTileSprite(i, j + 1))
						{
							possibleMatches.Add(new Tile(i - 1, j));
							possibleMatches.Add(new Tile(i, j + 1));
							possibleMatches.Add(new Tile(i, j + 2));
							return possibleMatches;
						}
						// Bottom
						if (j > 0)
						{
							if (GetTileSprite(i, j + 1) == GetTileSprite(i, j - 1))
							{
								possibleMatches.Add(new Tile(i, j - 1));
								possibleMatches.Add(new Tile(i, j + 1));
								possibleMatches.Add(new Tile(i, j + 2));
								return possibleMatches;
							}
						}
						// Right
						if (i < xSize - 1)
						{
							if (GetTileSprite(i, j + 1) == GetTileSprite(i + 1, j))
							{
								possibleMatches.Add(new Tile(i + 1, j));
								possibleMatches.Add(new Tile(i, j + 1));
								possibleMatches.Add(new Tile(i, j + 2));
								return possibleMatches;
							}
						}
					}
				}
				
				//X
				//O
				//X
				if (j < ySize - 2)
				{
					if (GetTileSprite(i, j) == GetTileSprite(i, j + 2))
					{
						// Left
						if (i > 0)
						{
							if (GetTileSprite(i, j) == GetTileSprite(i - 1, j + 1))
							{
								possibleMatches.Add(new Tile(i, j));
								possibleMatches.Add(new Tile(i - 1, j + 1));
								possibleMatches.Add(new Tile(i, j + 2));
								return possibleMatches;
							}
						}
						// Right
						if (i < xSize - 1)
						{
							if (GetTileSprite(i, j) == GetTileSprite(i + 1, j + 1))
							{
								possibleMatches.Add(new Tile(i, j));
								possibleMatches.Add(new Tile(i + 1, j + 1));
								possibleMatches.Add(new Tile(i, j + 2));
								return possibleMatches;
							}
						}
					}
				}
				
				// X X O
				if (i < xSize - 2)
				{
					if (GetTileSprite(i, j) == GetTileSprite(i + 1, j))
					{
						// Top
						if (j < ySize - 1)
						{
							if (GetTileSprite(i, j) == GetTileSprite(i + 2, j + 1))
							{
								possibleMatches.Add(new Tile(i, j));
								possibleMatches.Add(new Tile(i + 1, j));
								possibleMatches.Add(new Tile(i + 2, j + 1));
								return possibleMatches;
							}
						}
						// Right
						if (i < xSize - 3)
						{
							if (GetTileSprite(i, j) == GetTileSprite(i + 3, j))
							{
								possibleMatches.Add(new Tile(i, j));
								possibleMatches.Add(new Tile(i + 1, j));
								possibleMatches.Add(new Tile(i + 3, j));
								return possibleMatches;
							}
						}
						// Bottom
						if (j > 0)
						{
							if (GetTileSprite(i, j) == GetTileSprite(i + 2, j - 1))
							{
								possibleMatches.Add(new Tile(i, j));
								possibleMatches.Add(new Tile(i + 1, j));
								possibleMatches.Add(new Tile(i + 2, j - 1));
								return possibleMatches;
							}
						}
					}
				}
				
				// O X X
				if (i < xSize - 2)
				{
					if (GetTileSprite(i + 1, j) == GetTileSprite(i + 2, j))
					{
						// Top
						if (j < ySize - 1)
						{
							if (GetTileSprite(i + 1, j) == GetTileSprite(i, j + 1))
							{
								possibleMatches.Add(new Tile(i + 1, j));
								possibleMatches.Add(new Tile(i + 2, j));
								possibleMatches.Add(new Tile(i, j + 1));
								return possibleMatches;
							}
						}
						// Left
						if (i > 0)
						{
							if (GetTileSprite(i + 1, j) == GetTileSprite(i - 1, j))
							{
								possibleMatches.Add(new Tile(i + 1, j));
								possibleMatches.Add(new Tile(i + 2, j));
								possibleMatches.Add(new Tile(i - 1, j));
								return possibleMatches;
							}
						}
						// Bottom
						if (j > 0)
						{
							if (GetTileSprite(i + 1, j) == GetTileSprite(i, j - 1))
							{
								possibleMatches.Add(new Tile(i + 1, j));
								possibleMatches.Add(new Tile(i + 2, j));
								possibleMatches.Add(new Tile(i, j - 1));
								return possibleMatches;
							}
						}
					}
				}
				
				// X O X
				if (i < xSize - 2)
				{
					if (GetTileSprite(i, j) == GetTileSprite(i + 2, j))
					{
						// Top
						if (j < ySize - 1)
						{
							if (GetTileSprite(i, j) == GetTileSprite(i + 1, j + 1))
							{
								possibleMatches.Add(new Tile(i, j));
								possibleMatches.Add(new Tile(i + 2, j));
								possibleMatches.Add(new Tile(i + 1, j + 1));
								return possibleMatches;
							}
						}
						// Bottom
						if (j > 0)
						{
							if (GetTileSprite(i, j) == GetTileSprite(i + 1, j - 1))
							{
								possibleMatches.Add(new Tile(i, j));
								possibleMatches.Add(new Tile(i + 2, j));
								possibleMatches.Add(new Tile(i + 1, j - 1));
								return possibleMatches;
							}
						}
					}
				}
			}
		}

		return possibleMatches;
	}

	// Fill empty tile with new sprite
	public void FillEmptyTile(int x, int y)
	{
		if (GetTileSprite(x, y) == null || GetTileSprite(x, y) == transparentSprite)
		{
			GetTileManager(x, y).ResetTileState();

			if (Random.value > powerUpChance)
			{
				FillTileWithRandomCharacter(x, y);
			}
			else
			{
				FillTileWithPowerUp(x, y);
			}
		}
	}

	// Fill the tile with a random character
	public void FillTileWithRandomCharacter(int x, int y)
	{
		GetTileManager(x, y).IsPowerUp = false;
		SetTileSprite(x, y, characters[Random.Range(0, characters.Count)]);
	}

	// Fill tile with the selected power up
	public void FillTileWithPowerUp(int x, int y)
	{
		// TODO: retrieve powerup and set it
		GetTileManager(x, y).IsPowerUp = true;
		SetTileSprite(x, y, GameSettings.Instance.SelectedPowerUp);

	}

	public void OnPowerUpTouch(int x, int y)
	{
		TileManager tm = GetTileManager(x, y);
		if (tm.IsPowerUp == false) return;

		m_IsShifting = true;

		if (m_IsPowerUpBomb)
		{
			int radius = GameSettings.Instance.BombRadius;
			int points = 0;

			for (int i = -radius; i <= radius; i++)
			{
				for (int j = -radius; j <= radius; j++)
				{
					if (x + i < 0 || x + i >= xSize || y + j < 0 || y + j >= ySize) continue;
					SetTileSprite(x + i, y + j, transparentSprite);
					FillEmptyTile(x + i, y + j);
					points += GameSettings.Instance.SingleTilePoints;
				}
			}
			IncrementScore(points);
			ShouldCheckMatches = true;
		}
		else
		{
			// Freeze timer
			timer.FreezeTimer();
			SetTileSprite(x, y, transparentSprite);
			FillEmptyTile(x, y);
			ShouldCheckMatches = true;
		}

		m_IsShifting = false;
	}

	// Receive event when swipe is performed and manage swapping sprites
	public void OnTileSwipe(int x, int y, Vector2 swipeDirection)
	{	
		if (m_GameState != GameState.Playing)
		{
			return;
		}

		int adjX = x + (int)swipeDirection.x;
		int adjY = y + (int)swipeDirection.y;
		if (adjX >= 0 && adjX < xSize && adjY >= 0 && adjY < ySize)
		{
			m_IsShifting = true;

			Image shiftingFrom = GetTileImage(x, y);
			Image shiftingTo = GetTileImage(adjX, adjY);

			StartCoroutine(FadeSwap(shiftingFrom, shiftingTo));
		}
	}

	// Search for vertical matches in board
	private List<Match> SearchForVerticalMatches()
	{
		List<Match> matches = new List<Match>();

		for (int x = 0; x < xSize; x++)
		{
			for (int y = 0; y < ySize; y++)
			{
				// Start searching if current sprite matches tiles above
				int count = 1;
				Sprite sprite = GetTileSprite(x, y);

				while (y + count < ySize && sprite == GetTileSprite(x, y + count))
				{
					count++;
				}

				// If enough matches found, create a new match and add it to the list
				if(count >= m_MatchMinSize)
				{
					matches.Add(new Match(x, y, Vector2.up, count));
				}

				// Start from tile after last in just found match (y will be incremented by the floop)
				y = y + count - 1;
			}
		}

		return matches;
	}

	// Search for horizontal matches in board
	private List<Match> SearchForHorizontalMatches()
	{
		List<Match> matches = new List<Match>();

		for (int y = 0; y < ySize; y++)
		{
			for (int x = 0; x < xSize; x++)
			{
				// Start searching if current sprite matches tiles above
				int count = 1;
				Sprite sprite = GetTileSprite(x, y);

				while (x + count < ySize && sprite == GetTileSprite(x + count, y))
				{
					count++;
				}

				// If enough matches found, create a new match and add it to the list
				if (count >= m_MatchMinSize)
				{
					matches.Add(new Match(x, y, Vector2.right, count));
				}

				// Start from tile after last in just found match (x will be incremented by the floop)
				x = x + count - 1;
			}
		}

		return matches;
	}

	#endregion

	#region Graphics functions

	private IEnumerator SendPointsToScore(int points)
	{
		m_PointsAddedLabel.text = "+" + points.ToString();

		float startingX = m_PointsAddedLabel.rectTransform.anchoredPosition.x;
		float startingY = m_PointsAddedLabel.rectTransform.anchoredPosition.y;
		float distance = startingY - m_ScoreLabel.rectTransform.anchoredPosition.y;
		int increment = 5;

		m_PointsCanvasGroup.alpha = 0;

		for (int i = 0; i >= distance; i -= increment)
		{
			m_PointsCanvasGroup.alpha = (float)i / (distance);
			m_PointsAddedLabel.rectTransform.anchoredPosition = new Vector2(startingX, startingY - i);
			yield return null;
		}
		m_PointsAddedLabel.rectTransform.anchoredPosition = new Vector2(startingX, startingY);
		m_PointsCanvasGroup.alpha = 0;
		m_Score += points;
	}

	private IEnumerator FadeTileToTransparent(int x, int y)
	{

		Image img = GetTileImage(x, y);
		var tempColor = img.color;

		for (float f = 1f; f >= 0; f -= 0.1f)
		{
			tempColor.a = f;
			img.color = tempColor;

			yield return new WaitForSeconds(0.01f);
		}

		tempColor.a = 0.0f;
		img.color = tempColor;
		img.sprite = transparentSprite;
	}

	private IEnumerator FadeSwap(Image a, Image b)
	{

		yield return StartCoroutine(FadeOutSwap(a, b));

		Sprite tmp = b.sprite;
		b.sprite = a.sprite;
		a.sprite = tmp;

		yield return StartCoroutine(FadeInSwap(a, b));

		ShouldCheckMatches = true;

		m_IsShifting = false;
	}

	private IEnumerator FadeOutSwap(Image a, Image b)
	{
		var tempColor = a.color;

		for (float f = 1f; f >= 0; f -= 0.1f)
		{
			tempColor.a = f;
			a.color = tempColor;
			b.color = tempColor;

			yield return new WaitForSeconds(0.01f);
		}

		tempColor.a = 0.0f;
		a.color = tempColor;
		b.color = tempColor;
	}

	private IEnumerator FadeInSwap(Image a, Image b)
	{
		var tempColor = a.color;

		for (float f = 1f; f >= 0; f -= 0.1f)
		{
			tempColor.a = 1f - f;
			a.color = tempColor;
			b.color = tempColor;
			yield return new WaitForSeconds(0.01f);
		}

		tempColor.a = 1.0f;
		a.color = tempColor;
		b.color = tempColor;
	}

	#endregion

	#region Utilities

	// Create board with tiles of the proper dimension to fit screen width because game is played in portrait mode
	// Fill tiles with sprites without creating match-3 combination and inserting random power-ups
	private void CreateBoard()
	{
		RectTransform parentTransform = this.GetComponentInParent<RectTransform>();
		float boardWidth = parentTransform.sizeDelta.x;
		float tileWidth = (float)boardWidth / xSize;

		tiles = new GameObject[xSize, ySize];

		Sprite[] previousLeft = new Sprite[ySize];
		Sprite previousBelow = null;

		float horizontalOffset = 0.0f;
		for (int x = 0; x < xSize; x++)
		{
			if (x > 0)
			{
				horizontalOffset += tileWidth;
			}

			float verticalOffset = 0.0f;
			for (int y = 0; y < ySize; y++)
			{
				if (y > 0)
				{
					verticalOffset += tileWidth;
				}

				GameObject newTile = Instantiate(tile, new Vector3(horizontalOffset, verticalOffset, 0), transform.rotation);
				newTile.name = string.Format("[{0},{1}]", x, y);
				tiles[x, y] = newTile;

				TileManager newTileManager = newTile.GetComponentInChildren<TileManager>();
				newTileManager.xIndex = x;
				newTileManager.yIndex = y;

				Image newTileImage = newTile.GetComponentInChildren<Image>();
				newTileImage.rectTransform.pivot = Vector2.zero;
				newTileImage.rectTransform.anchorMin = Vector2.zero;
				newTileImage.rectTransform.anchorMax = Vector2.zero;
				newTileImage.rectTransform.anchoredPosition = new Vector2(horizontalOffset, verticalOffset);
				newTile.transform.SetParent(transform, false);


				List<Sprite> possibleCharacters = new List<Sprite>();
				possibleCharacters.AddRange(characters);

				possibleCharacters.Remove(previousLeft[y]);
				possibleCharacters.Remove(previousBelow);

				if (Random.value > powerUpChance)
				{
					Sprite newSprite = possibleCharacters[Random.Range(0, possibleCharacters.Count)];
					newTileImage.sprite = newSprite;
					previousLeft[y] = newSprite;
					previousBelow = newSprite;
				}
				else
				{
					newTileImage.sprite = m_PowerUpSprite;
					newTileManager.IsPowerUp = true;
				}

			}
		}

	}

	public TileManager GetTileManager(int x, int y)
	{
		if( x < 0 || x >= xSize || y < 0 || y > ySize)
		{
			Debug.LogErrorFormat("GetTileManager error: indexes out of bound exception. x: {0}, y: {1}", x, y);
			return null;
		} else
		{
			return tiles[x, y].GetComponent<TileManager>();
		}
	}


	public Image GetTileImage(int x, int y)
	{
		if (x < 0 || x >= xSize || y < 0 || y > ySize)
		{
			Debug.LogErrorFormat("GetTileImage error: indexes out of bound exception. x: {0}, y: {1}", x, y);
			return null;
		}
		else
		{
			return tiles[x, y].GetComponentInChildren<Image>();
		}
	}


	public Sprite GetTileSprite(int x, int y)
	{
		if (x < 0 || x >= xSize || y < 0 || y >= ySize)
		{
			Debug.LogErrorFormat("GetTileSprite error: indexes out of bound exception. x: {0}, y: {1}", x, y);
			return null;
		}
		else
		{
			return tiles[x, y].GetComponentInChildren<Image>().sprite;
		}
	}

	public void SetTileSprite(int x, int y, Sprite sprite)
	{
		if (x < 0 || x >= xSize || y < 0 || y >= ySize)
		{
			Debug.LogErrorFormat("SetTileSprite error: indexes out of bound exception. x: {0}, y: {1}", x, y);
		}
		else
		{
			tiles[x, y].GetComponentInChildren<Image>().sprite = sprite;
		}
	}

	#endregion

	#region Game flow management functions

	public void SetGameEnded()
	{
		if (m_GameState != GameState.Ended)
		{
			m_GameState = GameState.Ended;

			if (m_Score > GameSettings.Instance.Highscore)
			{
				GameSettings.Instance.SetHighscore(m_Score);
			}

			GameSettings.Instance.LastScore = m_Score;

			// Check for the last time if there are matches
			CheckMatches();

			m_GameOverEvent.Invoke();
		}
	}

	public void PauseGame()
	{
		if(m_GameState != GameState.Ended)
		{
			m_GameState = GameState.OnPause;
			
		}
	}

	public void PlayGame()
	{
		if (m_GameState != GameState.Ended)
			m_GameState = GameState.Playing;
	}

	#endregion
}

#region Helper classes

public enum GameState
{
	Playing,
	OnPause,
	Ended
}

public class Tile
{
	public int x;
	public int y;
	public Sprite tileSprite;

	public Tile(int x, int y)
	{
		this.x = x;
		this.y = y;
		this.tileSprite = null;
	}

	public Tile(int x, int y, Sprite sprite)
	{
		this.x = x;
		this.y = y;
		this.tileSprite = sprite;
	}
}

public class Match
{
	public int x;
	public int y;
	public Vector2 direction;
	public int matchCounter;
	public int points;

	private int smallMultiplier = GameSettings.Instance.SmallComboMultiplier;
	private int bigMultiplier = GameSettings.Instance.BigComboMultiplier;
	private int singleElementPoints = GameSettings.Instance.SingleTilePoints;

	public Match(int x, int y, Vector2 direction, int matchCounter)
	{
		this.x = x;
		this.y = y;
		this.direction = direction;
		this.matchCounter = matchCounter;
		points = singleElementPoints * matchCounter;
		if(matchCounter >= 5)
		{
			points *= bigMultiplier;
		} else if (matchCounter == 4)
		{
			points *= smallMultiplier;
		}
	}
}

#endregion