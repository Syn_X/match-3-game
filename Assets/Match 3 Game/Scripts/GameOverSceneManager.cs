﻿using System.Collections;
using UnityEngine;
using UnityEngine.SceneManagement;

public class GameOverSceneManager : MonoBehaviour
{
    public Animator crossfade;
    private float transitionTime = 0.5f;

    public void BackToMenu()
    {
        StartCoroutine(LoadNextScene());
    }

    private IEnumerator LoadNextScene()
    {
        crossfade.SetTrigger("CrossfadeStart");
        yield return new WaitForSeconds(transitionTime);

        SceneManager.LoadScene("MenuScene");
    }
}
