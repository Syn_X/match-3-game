﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class GameSettings : MonoBehaviour
{
    [Header("Game Settings")]
    [SerializeField]
    public int MatchMinSize = 3;
    [SerializeField]
    public int PlaytimeInSeconds = 0;

    [Header("Score Settings")]
    [SerializeField]
    public int SmallComboMultiplier = 2;
    [SerializeField]
    public int BigComboMultiplier = 3;
    [SerializeField]
    public int SingleTilePoints = 50;

    [Header("Board Settings")]
    [SerializeField]
    public int BoardWidth = 8;
    [SerializeField]
    public int BoardHeight = 8;

    [Header("Power Up Settings")]
    [SerializeField]
    public float PowerUpChance = 0.0f;
    [SerializeField]
    public int FreezeTime = 0;
    [SerializeField]
    public int BombRadius = 0;

    [HideInInspector]
    public Sprite SelectedPowerUp = null;
    [HideInInspector]
    public bool IsPowerUpBomb;
    [HideInInspector]
    public bool PlayerHasSelectedPowerUp = false;

    [HideInInspector]
    public int Highscore = 0;

    [HideInInspector]
    public int LastScore = 0;


    // Singleton pattern
    private static GameSettings _instance;
    public static GameSettings Instance
    {
        get
        {
            if (_instance == null)
            {
                _instance = GameObject.FindObjectOfType<GameSettings>();
            }

            return _instance;
        }
    }


    private void Awake()
    {
        // If I already exist, destroy me!
        if (_instance != null && _instance != this)
        {
            Destroy(this.gameObject);
        }
        else
        {
            _instance = this;
        }

        DontDestroyOnLoad(this.gameObject);

        Screen.sleepTimeout = SleepTimeout.NeverSleep;

    }

    // Start is called before the first frame update
    void Start()
    {
        // Retrieve highscore
        Highscore = PlayerPrefs.GetInt("Highscore", 0);
        LastScore = 0;

        if (BoardWidth <= 0) BoardWidth = 8;
        if (BoardHeight <= 0) BoardHeight = 8;

        // Clamp power up chance between 0 and 1
        if (PowerUpChance < 0f) PowerUpChance = 0f;
        if (PowerUpChance > 1f) PowerUpChance = 1f;

        if (MatchMinSize <= 0) MatchMinSize = 3;

        if (PlaytimeInSeconds <= 0) PlaytimeInSeconds = 60;

        if (FreezeTime <= 0) FreezeTime = 5;
        if (BombRadius <= 0) BombRadius = 1;

    }

    public void SetHighscore(int score)
    {
        Highscore = score;
        PlayerPrefs.SetInt("Highscore", score);
    }

}
