﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class HighscoreTextManager : MonoBehaviour
{

    private Text m_HighscoreText;
    private int m_Highscore = 0;

    // Start is called before the first frame update
    void Start()
    {
        m_Highscore = GameSettings.Instance.Highscore;
        m_HighscoreText = GetComponent<Text>();

        m_HighscoreText.text = "HIGHSCORE: " + m_Highscore.ToString();
    }

}
